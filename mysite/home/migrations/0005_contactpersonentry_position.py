# Generated by Django 3.2.12 on 2022-05-05 13:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_alter_homepage_headword'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactpersonentry',
            name='position',
            field=models.CharField(default='Mitarbeiter', max_length=255, verbose_name='Job-Bezeichnung'),
        ),
    ]
