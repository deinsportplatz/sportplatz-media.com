from .models import CustomerVoiceEntry, ContactPersonEntry, MarketingNewsEntry, ResourceDocument
from wagtail.api.v2.filters import OrderingFilter, FieldsFilter
from wagtail.api.v2.views import BaseAPIViewSet

class ResourceDocumentEndpoint(BaseAPIViewSet):
    model = ResourceDocument

    filter_backends = [OrderingFilter, FieldsFilter]

    body_fields = BaseAPIViewSet.body_fields + [
        'id',
        'title',
        'image',
        'description',
        'cta_url',
        'cta_text',
        'tags',
        'sortOrder',
        'target'
    ]

    listing_default_fields = BaseAPIViewSet.listing_default_fields = [
        'id',
        'title',
        'image',
        'description',
        'cta_url',
        'cta_text',
        'tags',
        'sortOrder',
        'target'
    ]

class CustomerVoiceEndpoint(BaseAPIViewSet):
    model = CustomerVoiceEntry

    body_fields = BaseAPIViewSet.body_fields + [
        'id',
        'citation',
        'customer',
        'image_url',
    ]

    listing_default_fields = BaseAPIViewSet.listing_default_fields = [
        'id',
        'citation',
        'customer',
        'image_url',
    ]

class ContactPersonEndpoint(BaseAPIViewSet):
    model = ContactPersonEntry
    
    body_fields = BaseAPIViewSet.body_fields + [
        'id',
        'name',
        'email',
        'telefon',
	    'position',
        'hubspot_form_id',
        'image_url'
    ]

    listing_default_fields = BaseAPIViewSet.listing_default_fields = [
        'id',
        'name',
        'email',
        'telefon',
        'image_url'
    ]


class MarketingNewsEndpoint(BaseAPIViewSet):
    model = MarketingNewsEntry

    body_fields = BaseAPIViewSet.body_fields + [
        'id',
        'title',
        'news',
        'category',
        'image_url'
    ]

    listing_default_fields = BaseAPIViewSet.listing_default_fields = [
        'id',
        'title',
        'news',
        'category',
        'image_url'
    ]

