from pydoc import classname
from tabnanny import verbose
from django.db import models

from wagtail.models import Page
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel
from wagtail.api import APIField
from wagtail.images import get_image_model_string

from wagtail.snippets.models import register_snippet
import datetime

from taggit.models import TaggedItemBase
from modelcluster.models import ClusterableModel
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey

class HomePage(Page):
    body = RichTextField(blank=True)
    headword = models.CharField(max_length=255)
    date = models.DateField("Post date", default=datetime.date.today)

    image = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
    
    def image_url(self):
        if self.image is not None:
            return self.image.filename
    
    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        FieldPanel('image'),
	FieldPanel('headword'),
	FieldPanel('date')
    ]

    api_fields = [
        APIField('body'),
        APIField('image_url'),
        APIField('image'),
	APIField('headword'),
	APIField('date')
    ]


@register_snippet
class CustomerVoiceEntry(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
    citation = models.CharField(max_length=500, verbose_name='Zitat')
    customer = models.CharField(max_length=255, verbose_name='Name und Kunde')

    panels = [
        FieldPanel('citation'),
        FieldPanel('customer'),
        FieldPanel('image')
    ]

    def __str__(self):
        return self.citation

    def image_url(self):
        if self.image is not None:
            return self.image.filename

    class Meta:
        verbose_name_plural = 'customer voices'

@register_snippet
class ContactPersonEntry(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
    name = models.CharField(max_length=255, verbose_name='Vor & Nachname')
    email = models.CharField(max_length=500, verbose_name='Email-Addresse')
    telefon = models.CharField(max_length=255, verbose_name='Telefonnummer')
    position = models.CharField(max_length=255, verbose_name='Job-Bezeichnung', default='Mitarbeiter')
    hubspot_form_id = models.CharField(max_length=255, verbose_name='Hubspot Formular Id', default="")

    panels = [
        FieldPanel('name'),
        FieldPanel('email'),
        FieldPanel('telefon'),
	    FieldPanel('position'),
        FieldPanel('hubspot_form_id'),
        FieldPanel('image')
    ]

    def __str__(self):
        return self.name

    def image_url(self):
        if self.image is not None:
            return self.image.filename

    class Meta:
        verbose_name_plural = 'contact persons'

@register_snippet
class MarketingNewsEntry(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
    title = models.CharField(max_length=255, verbose_name='News-Headline')
    news =  models.CharField(max_length=500, verbose_name='News-Text')
    category = models.CharField(max_length=255, verbose_name='News-Kategorie')

    panels = [
        FieldPanel('title'),
        FieldPanel('news'),
        FieldPanel('category'),
        FieldPanel('image')
    ]

    def __str__(self):
        return self.title

    def image_url(self):
        if self.image is not None:
            return self.image.filename

    class Meta:
        verbose_name_plural = 'marketing news'

class ResourceDocumentTag(TaggedItemBase):
    content_object = ParentalKey(
        'ResourceDocument',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )

@register_snippet
class ResourceDocument(ClusterableModel):
    TARGET_SELF = 1
    TARGET_BLANK = 2
    Targets = (
        (TARGET_SELF, "aktuelles Fenster"),
        (TARGET_BLANK, "neues Fenster"),
    )

    id = models.AutoField(primary_key=True)
    image = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
    title = models.CharField(max_length=255, verbose_name='Titel')
    cta_url = models.CharField(max_length=255, verbose_name='Ziel-URL')
    cta_text = models.CharField(max_length=255, verbose_name='Call-to-Action')
    description =  RichTextField(blank=True, verbose_name='Beschreibung')
    tags = ClusterTaggableManager(through=ResourceDocumentTag, blank=True)
    sortOrder = models.IntegerField(null=True, verbose_name="Sortierung")
    target = models.IntegerField(choices=Targets, default=TARGET_BLANK, verbose_name="Link öffnen")

    panels = [
        FieldPanel('title'),
        FieldPanel('image'),
        FieldPanel('description'),
        FieldPanel('cta_url'),
        FieldPanel('cta_text'),
        FieldPanel('tags'),
        FieldPanel('target'),
        FieldPanel('sortOrder')
    ]

    def __str__(self):
        return self.title

    def image_url(self):
        if self.image is not None:
            return self.image.filename

    class Meta:
        verbose_name_plural = 'resource documents'
        ordering =  ["sortOrder"]
