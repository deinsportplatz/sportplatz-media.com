from django.db import models

# Create your models here.
from wagtail.images.models import Image, AbstractImage, AbstractRendition
from wagtail.api import APIField

class CustomImage(AbstractImage):
    id = models.AutoField(primary_key=True)
    # Add any extra fields to image here

    # eg. To add a caption field:
    caption = models.CharField(max_length=255, blank=True)
    copyright = models.CharField(max_length=255, blank=True)

    admin_form_fields = Image.admin_form_fields + (
        # Then add the field names here to make them appear in the form:
        'caption', 'copyright'
    )

    api_fields = [
        APIField('caption'),
        APIField('copyright'),
    ]


class CustomRendition(AbstractRendition):
    id = models.AutoField(primary_key=True)
    image = models.ForeignKey(CustomImage, on_delete=models.CASCADE, related_name='renditions')

    class Meta:
        unique_together = (
            ('image', 'filter_spec', 'focal_point_key'),
        )
