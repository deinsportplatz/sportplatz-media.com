from email.policy import default
from tabnanny import verbose
from django.db import models

from wagtail import blocks
from wagtail.images.blocks import ImageChooserBlock

from wagtail.models import Page, Orderable
from wagtail.fields import RichTextField, StreamField
from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.images import get_image_model_string
from wagtail.api import APIField
from modelcluster.fields import ParentalKey
from wagtail.search import index


# Create your models here.

class BlogPage(Page):

    header_text = RichTextField(blank=True)
    header_cta = StreamField([
        ('cta_styles', blocks.StructBlock([
            ('color', blocks.CharBlock()),
            ('bg_color', blocks.CharBlock()),
            ('arrow', blocks.BooleanBlock(required=False)),
            ('custom_css', blocks.RichTextBlock(required=False))
        ])),
        ('cta_text', blocks.CharBlock(form_classname='title')),
        ('cta_link', blocks.CharBlock()),
        ('cta_target_blank', blocks.BooleanBlock(required=False)),
    ], use_json_field=True, blank=True, verbose_name="Header CTA Informationen")

    body = RichTextField(blank=True)
    image = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )

    def image_url(self):
        if self.image is not None:
            return self.image.filename

    def image_copyright(self):
        if self.image is not None:
            return self.image.copyright

    content_panels = Page.content_panels + [
        FieldPanel('header_text'),
        FieldPanel('header_cta'),
        FieldPanel('body', classname="full"),
        FieldPanel('image'),
        InlinePanel('related_tiles', label="Kacheln"),
    ]

    api_fields = [
        APIField('header_text'),
        APIField('header_cta'),
        APIField('body'),
        APIField('image_url'),
        APIField('image_copyright'),
        APIField('related_tiles'),
    ]

class BlogPageTile(Orderable):
    page = ParentalKey(BlogPage, on_delete=models.CASCADE, related_name='related_tiles', default=None)
    image_tile = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
    title_tile = models.CharField(blank=True, max_length=255, verbose_name='Kachel-Title')
    subhead_tile = models.CharField(blank=True, max_length=255, verbose_name='Sub-Headline')
    body_tile = RichTextField(blank=True, verbose_name='Kachel-Inhalt')
    url_tile = models.URLField(blank=True)
    position_left = models.BooleanField(null=True, default=True, verbose_name='Position - Links')

    # cta
    cta = StreamField([
        ('cta_styles', blocks.StructBlock([
            ('color', blocks.CharBlock()),
            ('bg_color', blocks.CharBlock()),
            ('arrow', blocks.BooleanBlock(required=False)),
            ('custom_css', blocks.RichTextBlock(required=False))
        ])),
        ('cta_text', blocks.CharBlock(form_classname='title')),
        ('cta_link', blocks.CharBlock()),
        ('cta_target_blank', blocks.BooleanBlock(required=False)),
    ], use_json_field=True, blank=True, verbose_name="CTA Informationen")


    panels = [
        FieldPanel('title_tile'),
        FieldPanel('subhead_tile'),
        FieldPanel('body_tile'),
        FieldPanel('image_tile'),
        FieldPanel('url_tile'),
        FieldPanel('position_left'),
        FieldPanel('cta', classname="full")
    ]
    api_fields = [
        APIField("title_tile"),
        APIField("subhead_tile"),
        APIField("body_tile"),
        APIField("image_tile"),
        APIField("url_tile"),
        APIField("position_left"),
        APIField("cta"),
    ]

class BlogPageProducts(Orderable):
    page = ParentalKey(BlogPage, on_delete=models.CASCADE, related_name='related_products', default=None)
    product_logo = models.ForeignKey(
        get_image_model_string(), null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
