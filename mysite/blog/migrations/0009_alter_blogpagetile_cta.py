# Generated by Django 3.2.12 on 2023-11-28 13:35

from django.db import migrations
import wagtail.blocks
import wagtail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20231128_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpagetile',
            name='cta',
            field=wagtail.fields.StreamField([('cta_styles', wagtail.blocks.StructBlock([('color', wagtail.blocks.CharBlock()), ('bg_color', wagtail.blocks.CharBlock()), ('arrow', wagtail.blocks.BooleanBlock(required=False)), ('custom_css', wagtail.blocks.RichTextBlock(required=False))])), ('cta_text', wagtail.blocks.CharBlock(form_classname='title')), ('cta_link', wagtail.blocks.CharBlock())], blank=True, use_json_field=True, verbose_name='CTA Informationen'),
        ),
    ]
