# api.py
from wagtail.api.v2.views import PagesAPIViewSet
from wagtail.api.v2.router import WagtailAPIRouter
from wagtail.images.api.v2.views import ImagesAPIViewSet
from wagtail.documents.api.v2.views import DocumentsAPIViewSet
from home.endpoints import CustomerVoiceEndpoint, ContactPersonEndpoint, MarketingNewsEndpoint, ResourceDocumentEndpoint

# create router
api_router = WagtailAPIRouter('wagtailapi')

api_router.register_endpoint('pages', PagesAPIViewSet)
api_router.register_endpoint('images', ImagesAPIViewSet)
api_router.register_endpoint('documents', DocumentsAPIViewSet)
api_router.register_endpoint('customers', CustomerVoiceEndpoint)
api_router.register_endpoint('contacts', ContactPersonEndpoint)
api_router.register_endpoint('news', MarketingNewsEndpoint)
api_router.register_endpoint('resources', ResourceDocumentEndpoint)

