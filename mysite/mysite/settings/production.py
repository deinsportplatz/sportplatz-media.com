from .base import *

DEBUG = True
ALLOWED_HOSTS = ['backend.sportplatz-media.com', 'sportplatz-media.com', 'localhost']

try:
    from .local import *
except ImportError:
    pass
